---
title: Signoff Website
---

Welcome to The Pins Team's **Signoff**, an website documentating the processes on how to sign [our CLA](/contributor-agreement/index.md), among other things.

## Sign our unified CLA now

Choose your path, follow the instructions to finish the paperworks, and now you're ready to contribute.

### Signing as individual?

[Sign the Individual CLA](contributor-agreement/sign-individual.md){ .md-button .md-button--primary }

### Signing as an entity?

> :warning: We're working on how the process works for entities right now. Please come back soon.

[Sign the Entity CLA](contributor-agreement/sign-entity.md){ .md-button }

## Documentation

TBD
